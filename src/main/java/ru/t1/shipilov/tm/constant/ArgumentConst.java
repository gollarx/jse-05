package ru.t1.shipilov.tm.constant;

public final class ArgumentConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    private ArgumentConst() {
    }

}
